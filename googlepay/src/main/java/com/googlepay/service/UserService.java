package com.googlepay.service;

import com.googlepay.dto.UserRequestDto;

public interface UserService {

	String saveUser(UserRequestDto userRequestDto);

}
